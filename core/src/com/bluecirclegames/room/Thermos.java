package com.bluecirclegames.room;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;

public class Thermos {
    private Thermo[] thermos;
    int population = 0;

    RoomsGame game;

    public Thermos(int population, RoomsGame game) {
        this.population = population;
        this.thermos = new Thermo[population];

        this.game = game;

        for (int i = 0; i < population; i++) {
            int x = MathUtils.random(game.room_width - 1);
            int y = MathUtils.random(game.room_height - 1);
            this.thermos[i] = new Thermo(x, y, game);
        }
    }

    public void render(float delta, SpriteBatch batch) {
        for (int i = 0; i < thermos.length; i++) {
            thermos[i].render(delta, batch);
        }
    }

    public void move(){
        for (int i = 0; i < thermos.length; i++) {
            thermos[i].moveToRandomRoom();
        }
    }
}
