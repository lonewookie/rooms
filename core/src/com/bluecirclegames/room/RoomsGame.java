package com.bluecirclegames.room;

import com.badlogic.gdx.Game;

public class RoomsGame extends Game {

    int total_rounds;
    int current_round;

    int safe_room_x = 0;
    int safe_room_y = 0;
    int room_width = 0;
    int room_height = 0;
	
	@Override
	public void create () {
        total_rounds = 10;
        current_round = 1;
        setScreen(new SplashScreen(this));
	}
}
