package com.bluecirclegames.room;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Timer;

public class Room {
    private int x;
    private int y;
    private Texture img;
    private Texture burnt_room;
    Texture alt;
    boolean is_exploding = false;
    int explosion_delay = 0;
    boolean has_exploded = false;
    boolean is_safe = false;

    // Animation stuff
    private float elapsed_time = 0;
    private int blink_time = 0;
    Texture bomb_sheet;
    TextureRegion bomb_texture_region;
    TextureRegion[] bomb_frames;
    Animation bomb_animation;

    RoomsGame game;

    public Room(int x, int y, boolean is_safe_room, RoomsGame game) {
        this.x = x;
        this.y = y;
        this.img = new Texture("room.png");
        this.burnt_room = new Texture("burnt_room.png");
        this.is_safe = is_safe_room;

        alt = new Texture("win.png");

        this.game = game;

        // Animation stuff
        bomb_sheet = new Texture("bomb_sheet.png");
        TextureRegion[][] tmp = TextureRegion.split(bomb_sheet, bomb_sheet.getWidth() / 2, bomb_sheet.getHeight() / 2);
        bomb_frames = new TextureRegion[4];

        int index = 0;
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                bomb_frames[index++] = tmp[i][j];
            }
        }
        bomb_animation = new Animation(0.1f, bomb_frames);
    }

    public void render(float delta, SpriteBatch batch) {
        if (has_exploded) {
            batch.draw(burnt_room, this.x * img.getWidth(), this.y * img.getHeight());
        } else {
            batch.draw(img, this.x * img.getWidth(), this.y * img.getHeight());
        }

        if (is_exploding) {
            if (explosion_delay > 0) {
                explosion_delay--;
            } else {
                elapsed_time += Gdx.graphics.getDeltaTime();
                batch.draw(bomb_animation.getKeyFrame(elapsed_time, false), this.x * img.getWidth(), this.y * img.getHeight());
                blink_time++;

                if (bomb_animation.isAnimationFinished(elapsed_time)) {
                    is_exploding = false;
                    explosion_delay = 0;
                    has_exploded = true;
                };
            }
        }
    }

    public void setIsExploding(boolean is_exploding, int delay) {
        if (!is_safe) {
            this.is_exploding = is_exploding;
            this.explosion_delay = delay;
        }
    }
}
