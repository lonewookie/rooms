package com.bluecirclegames.room;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;

public class Thermo {
    private int x;
    private int y;
    private Texture blue;
    private Texture red;
    private RoomsGame game;

    public Thermo(int x, int y, RoomsGame game) {
        this.x = x;
        this.y = y;
        this.blue = new Texture("blue_thermo.png");
        this.red = new Texture("red_thermo.png");
        this.game = game;
    }

    public void render(float delta, SpriteBatch batch) {
        if (this.IsNearSafeRoom(game.safe_room_x, game.safe_room_y)) {
            batch.draw(blue, this.x * blue.getWidth(), this.y * blue.getHeight());
        } else {
            batch.draw(red, this.x * red.getWidth(), this.y * red.getHeight());
        }
    }

    public void moveToRandomRoom() {
        int new_x = x;
        int new_y = y;

        int dir = MathUtils.random(0, 3);

        switch (dir) {
            case 0: // Up
                if (this.y != game.room_height - 1) {
                    new_y += 1;
                }
                break;
            case 1: // Right
                if (this.y != game.room_width - 1) {
                    new_x +=1;
                }
                break;
            case 2: // Down
                if (this.y != 0) {
                    new_y -= 1;
                }
                break;
            case 3: // Left
                if (this.y != 0) {
                    new_x -= 1;
                }
                break;
        }

        if (new_x < game.room_width && new_x >= 0 && new_y < game.room_height && new_y >= 0) {
            this.setPosition(new_x, new_y);
        } else {
        }
    }

    public void setPosition(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public boolean IsNearSafeRoom(int safe_room_x, int safe_room_y) {
        int x_difference = Math.abs(this.getX() - safe_room_x);
        int y_difference = Math.abs(this.getY() - safe_room_y);

        if (x_difference < 2 && y_difference < 2) {
            return true;
        }

        return false;
    }
}
