package com.bluecirclegames.room;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.viewport.FitViewport;

public class SplashScreen extends InputAdapter implements Screen {
    RoomsGame game;
	SpriteBatch batch;
	Texture img;
    Texture touch;
    OrthographicCamera cam;
    FitViewport game_view;
    Vector2 touch_position;

    Rooms rooms;
    Guy guy;

    public SplashScreen(RoomsGame game) {
        this.game = game;
        cam = new OrthographicCamera(Constants.WIDTH, Constants.HEIGHT);
        game_view = new FitViewport(Constants.WIDTH, Constants.HEIGHT, cam);

        this.img = new Texture("guy.png");
        this.touch = new Texture("touch.png");
        batch = new SpriteBatch();

        touch_position = new Vector2(0, 0);
    }

	public void render (float delta) {
        // Clear the screen
        Gdx.gl.glClearColor(255, 255, 255, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // Set up the camera stuff
        game_view.apply();
        cam = (OrthographicCamera) game_view.getCamera();

        // Set the projection matrices (whatever that means)
        batch.setProjectionMatrix(cam.combined);

        batch.begin();
        batch.draw(img, Constants.WIDTH / 2 - img.getWidth() / 2, Constants.HEIGHT / 2 - img.getHeight() / 2);
        batch.draw(touch, Constants.WIDTH / 2 - img.getWidth() / 2, Constants.HEIGHT / 2 - img.getHeight() / 2 - 32);
        batch.end();

        if(Gdx.input.justTouched()) {
            //Vector3 new_touch_position = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0);
            game.setScreen(new GameScreen(this.game));
        }
	}

    @Override
    public void resize(int width, int height) {
        game_view.update(width, height, true);
    }

    @Override
    public void dispose() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void show() {
    }

    @Override
    public void hide() {
    }
}
