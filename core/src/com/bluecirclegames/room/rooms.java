package com.bluecirclegames.room;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.actions.DelayAction;

public class Rooms {
    private int width;
    private int height;
    private Room[][] rooms;

    RoomsGame game;

    public Rooms(int width, int height, int safe_room_x, int safe_room_y, RoomsGame game) {
        this.width = width;
        this.height = height;
        this.rooms =  new Room[width][height];

        this.game = game;

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                boolean is_safe_room = false;
                if (i == safe_room_x && j == safe_room_y) {
                    is_safe_room = true;
                }

                this.rooms[i][j] = new Room(i, j, is_safe_room, game);
            }
        }
    }

    public void render(float delta, SpriteBatch batch) {
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                rooms[i][j].render(delta, batch);
            }
        }
    }

    public void destroyRooms() {
        float elapsed_time = 0;
        for (int i = 0; i < this.width; i++) {
            for (int j = 0; j < this.height; j++) {
                rooms[i][j].setIsExploding(true, (i * 5 + j * 3));
            }
        }
    }
}
