package com.bluecirclegames.room;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;

public class Guy {
    private int x;
    private int y;
    private Texture img;
    private RoomsGame game;

    public Guy(int x, int y, RoomsGame game) {
        this.x = x;
        this.y = y;
        this.img = new Texture("guy.png");
        this.game = game;
    }

    public void render(float delta, SpriteBatch batch) {
        batch.draw(img, this.x * img.getWidth(), this.y * img.getHeight());
    }

    public void setPosition(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public boolean IsInSafeRoom() {
        if (x == game.safe_room_x && y == game.safe_room_y) {
            return true;
        }

        return false;
    }

    public boolean IsNearSafeRoom(int safe_room_x, int safe_room_y) {
        int x_difference = Math.abs(this.getX() - safe_room_x);
        int y_difference = Math.abs(this.getY() - safe_room_y);
        if (x_difference < 2 && y_difference < 2) {
            return true;
        }

        return false;
    }
}
