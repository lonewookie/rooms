package com.bluecirclegames.room;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.viewport.FitViewport;

public class GameScreen extends InputAdapter implements Screen {
    RoomsGame game;
	SpriteBatch batch;
	Texture img;
    OrthographicCamera cam;
    FitViewport game_view;
    Vector2 touch_position;

    BitmapFont font;

    Texture win_img;
    Texture lose_img;

    Rooms rooms;
    Guy guy;
    Thermos thermos;
    VerticalThermos vertical_thermos;
    HorizontalThermos horizontal_thermos;

    public GameScreen(RoomsGame game) {
        this.game = game;
        cam = new OrthographicCamera(Constants.WIDTH, Constants.HEIGHT);
        cam.position.set(100f, 100f, 0f);
        game_view = new FitViewport(Constants.WIDTH, Constants.HEIGHT, cam);
        batch = new SpriteBatch();

        //font = new BitmapFont(Gdx.files.internal("font.fnt"), Gdx.files.internal("font_image.png"), false);
        font = new BitmapFont();

        win_img = new Texture("win.png");
        lose_img = new Texture("lose.png");

        int safe_room_x = MathUtils.random(5 - 1);
        int safe_room_y = MathUtils.random(5 - 1);

        game.safe_room_x = safe_room_x;
        game.safe_room_y = safe_room_y;
        game.room_width = 5;
        game.room_height = 5;

        rooms = new Rooms(5, 5, game.safe_room_x, game.safe_room_y, game);
        guy = new Guy(0, 0, game);
        thermos = new Thermos(2, game);
        vertical_thermos = new VerticalThermos(2, game);
        horizontal_thermos = new HorizontalThermos(2, game);

        touch_position = new Vector2(0, 0);

        game.current_round = 1;
    }

	public void render (float delta) {
        // Clear the screen
        Gdx.gl.glClearColor(255, 255, 255, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // Set up the camera stuff
        game_view.apply();
        cam = (OrthographicCamera) game_view.getCamera();

        // Set the projection matrices (whatever that means)
        batch.setProjectionMatrix(cam.combined);

        batch.begin();
        font.setColor(0.0f, 0.0f, 0.0f, 1.0f);
        font.draw(batch, "Turns Left: " + (game.total_rounds - game.current_round), 15, Constants.HEIGHT - 20);
        batch.end();

        batch.begin();
        rooms.render(delta, batch);
        thermos.render(delta, batch);
        vertical_thermos.render(delta, batch);
        horizontal_thermos.render(delta, batch);
        guy.render(delta, batch);

        if (game.current_round > game.total_rounds) {
            rooms.destroyRooms();
            game.current_round = 0;
        }

        if (game.current_round == 0) {
            if (guy.IsInSafeRoom()) {
                batch.draw(
                        win_img,
                        Constants.WIDTH / 2 - win_img.getWidth() / 2,
                        Constants.HEIGHT / 2 - win_img.getHeight() / 2
                );
            } else {
                batch.draw(
                        lose_img,
                        Constants.WIDTH / 2 - lose_img.getWidth() / 2,
                        Constants.HEIGHT / 2 - lose_img.getHeight() / 2
                );
            }

            if(Gdx.input.justTouched()) {
                game.setScreen(new SplashScreen(game));
            }
        } else {
            if(Gdx.input.justTouched()) {
                Vector3 new_touch_position = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0);

                game_view.unproject(new_touch_position);
                touch_position = new Vector2(new_touch_position.x, new_touch_position.y);
                int new_x = (int) touch_position.x / 32;
                int new_y = (int) touch_position.y / 32;
                int x_difference = Math.abs(guy.getX() - new_x);
                int y_difference = Math.abs(guy.getY() - new_y);
                if (x_difference + y_difference < 2) {
                    guy.setPosition(new_x, new_y);
                    thermos.move();
                    vertical_thermos.move();
                    horizontal_thermos.move();
                    game.current_round++;
                } else {
                    Gdx.app.log("error", "Can't move to this space.");
                }
            }
        }

        batch.end();
	}

    @Override
    public void resize(int width, int height) {
        game_view.update(width, height, true);
    }

    @Override
    public void dispose() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void show() {
    }

    @Override
    public void hide() {
    }
}
