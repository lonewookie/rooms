package com.bluecirclegames.room;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;

public class HorizontalThermo {
    private int x;
    private int y;
    private Texture left_img;
    private Texture right_img;
    private Texture center_img;
    private RoomsGame game;

    public HorizontalThermo(int x, int y, RoomsGame game) {
        this.x = x;
        this.y = y;
        this.left_img = new Texture("horizontal_left.png");
        this.right_img = new Texture("horizontal_right.png");
        this.center_img = new Texture("horizontal_center.png");
        this.game = game;
    }

    public void render(float delta, SpriteBatch batch) {
        if (game.safe_room_x < this.getX()) {
            batch.draw(left_img, this.x * 32, this.y * 32);
        } else if (game.safe_room_x > this.getX()) {
            batch.draw(right_img, this.x * 32, this.y * 32);
        } else {
            batch.draw(center_img, this.x * 32, this.y * 32);
        }
    }

    public void moveToRandomRoom() {
        int new_x = x;
        int new_y = y;

        int dir = MathUtils.random(0, 3);

        switch (dir) {
            case 0: // Up
                if (this.y != game.room_height - 1) {
                    new_y += 1;
                }
                break;
            case 1: // Right
                if (this.y != game.room_width - 1) {
                    new_x +=1;
                }
                break;
            case 2: // Down
                if (this.y != 0) {
                    new_y -= 1;
                }
                break;
            case 3: // Left
                if (this.y != 0) {
                    new_x -= 1;
                }
                break;
        }

        if (new_x < game.room_width && new_x >= 0 && new_y < game.room_height && new_y >= 0) {
            this.setPosition(new_x, new_y);
        } else {
        }
    }

    public void setPosition(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
